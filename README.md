# SKA Angular Webapp Skeleton

This project is intended to act as a skeleton for any SKA developer looking to make an Angular based web application.

It includes tools for linting, code formatting, and testing which are easily integrated into various IDEs.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

It uses Angular Material UI as the core component library.

## Development server

Run `yarn start` for a dev server. Navigate to `http://localhost:8091/`. The app will automatically reload if you change any of the source files.

The necessary steps are:

Install Git if you don't already have it.

To find if Git is installed on your computer, type in a terminal: git --version. The output will either say which version of Git is installed, or that git is an unknown command.
If Git is not there, point your browser to `https://git-scm.com/book/en/v2/Getting-Started-Installing-Git` and follow the instructions for installation`.

Clone the skeleton project from the SKA Git repository by moving to the destination directory on your machine, and typing:
```
git clone https://gitlab.com/ska-telescope/templates/ska-angular-webapp-skeleton.git.
``` 

Run yarn start for a dev server. Navigate to http://localhost:8091.

Enter the code directory with: 
```
cd ska-angular-webapp-skeleton
```

## Make Commands

Inside the root of the project you can use:

```
make up
```

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Adding Translation

To add translation to the application, you should first add the field to the en.json file within `/assets/i18n/`. This should be in the format `translate.COMPONENT_NAME.FIELD_NAME` for consistency. Then you just need to reference that in the template file making sure to add 'translate' to the html tag.
The exact same field should then be added to the other language files that exist with the corresponding translation files e.g. fr.json. 