PROJECT_NAME = ska-angular-webapp-skeleton

# include makefile targets from the submodule
include .make/base.mk
include .make/oci.mk
include .make/k8s.mk
include .make/helm.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

OCI_IMAGE_BUILD_CONTEXT = $(PWD)

#needs replaced with oci-build and dockerfile eventually
up: 
	yarn install;
	yarn start;