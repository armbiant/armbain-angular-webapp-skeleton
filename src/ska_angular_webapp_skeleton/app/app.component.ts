import { Component } from '@angular/core';
import { AppConfigService } from './shared/services/app-config.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedService } from './shared/services/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  static readonly DARK_THEME_CLASS = 'dark-theme';
  static readonly LOCAL_STORAGE_TITLE = 'skao-theme';

  constructor(
    private configuration: AppConfigService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private sharedService: SharedService
  ) {
    this.checkThemePreference();
    this.registerMatIcons();
    this.sharedService.lightMode.subscribe(() => this.selectLightTheme());
    this.sharedService.darkMode.subscribe(() => this.selectDarkTheme());
  }

  checkThemePreference(): void {
    const currentTheme = localStorage.getItem(AppComponent.LOCAL_STORAGE_TITLE);
    if (currentTheme == AppComponent.DARK_THEME_CLASS) {
      document.documentElement.classList.add(AppComponent.DARK_THEME_CLASS);
    }
  }

  selectDarkTheme(): void {
    localStorage.setItem(
      AppComponent.LOCAL_STORAGE_TITLE,
      AppComponent.DARK_THEME_CLASS
    );
    document.documentElement.classList.add(AppComponent.DARK_THEME_CLASS);
  }

  selectLightTheme(): void {
    localStorage.removeItem(AppComponent.LOCAL_STORAGE_TITLE);
    document.documentElement.classList.remove(AppComponent.DARK_THEME_CLASS);
  }

  getConfiguration() {
    return this.configuration.getConfiguration();
  }

  registerMatIcons() {
    this.iconRegistry.addSvgIcon(
      'en',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/en.svg')
    );
    this.iconRegistry.addSvgIcon(
      'de',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/de.svg')
    );
    this.iconRegistry.addSvgIcon(
      'fr',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/fr.svg')
    );
  }
}
