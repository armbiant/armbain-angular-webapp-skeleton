import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  year: string;

  constructor() {
    this.year = '';
  }

  ngOnInit(): void {
    this.year = new Date().getFullYear().toString();
  }

  getYear(): string {
    return this.year;
  }

  getVersion(): string {
    //TODO: A nicer way to retrieve this automatically on release
    return '0.0.0';
  }
}
