import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public lightMode = new EventEmitter<boolean>();
  public darkMode = new EventEmitter<boolean>();

  constructor() { }

}
