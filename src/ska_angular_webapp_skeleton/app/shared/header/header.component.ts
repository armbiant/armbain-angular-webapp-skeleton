import { Component, OnInit } from '@angular/core';
import { Languages } from '../models/language.models';
import { AppComponent } from '../../app.component';
import { AppConfigService } from '../services/app-config.service';
import { TranslationService } from '../services/translation.service';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  configurationVal!: any;
  languages?: Languages[];
  selectedLanguageCode: string = 'en';
  selectedLanguageTitle: string = 'en';

  constructor(
    private translate: TranslationService,
    private configuration: AppConfigService,
    private sharedService: SharedService
  ) {}

  ngOnInit(): void {
    this.configurationVal = this.configuration.getConfiguration();
    this.languages = this.getLanguages();
  }

  useLanguage(language: Languages): void {
    this.translate.useLanguage(language.code);
    this.selectedLanguageCode = language.code;
    this.selectedLanguageTitle = language.title;
  }

  getLanguages(): Languages[] {
    return this.translate.getAvailableLanguages();
  }

  goToDocumentation() {
    window.open(this.configurationVal.documentationURL, '_blank');
  }

  isDarkMode(): boolean {
    const currentTheme = localStorage.getItem(AppComponent.LOCAL_STORAGE_TITLE);
    if (currentTheme) {
      return true;
    }
    return false;
  }

  swapTheme(): void {
    if (this.isDarkMode()) {
      this.selectLightTheme();
    } else {
      this.selectDarkTheme();
    }
  }

  selectLightTheme() {
    this.sharedService.lightMode.emit(true);
  }

  selectDarkTheme() {
    this.sharedService.darkMode.emit(true);
  }
}
