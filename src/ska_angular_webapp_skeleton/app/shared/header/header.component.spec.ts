import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header.component';
import { AppConfigService } from '../services/app-config.service';
import { TranslationService } from '../services/translation.service';
import { TranslateModule } from '@ngx-translate/core';
import * as config from '../../../assets/configuration/configuration.json';
import { MatMenuModule } from '@angular/material/menu';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let transService: MockService;

  beforeEach(async () => {
    transService = new MockService();
    await TestBed.configureTestingModule({
      imports: [HttpClientModule, MatMenuModule, TranslateModule.forRoot()],
      declarations: [HeaderComponent],
      providers: [
        AppConfigService,
        { provide: TranslationService, useValue: transService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    component.configurationVal = config;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

class MockService {
  getAvailableLanguages() {}
}
